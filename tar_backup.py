#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""Automates incremental GNU tar backups. Not compatible with BSDTAR in any way.

"""


import sys 
import argparse
import yaml
import os
import subprocess
import re
from collections import OrderedDict
import contextlib
import errno


CONFIG_FILE_PATH = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    'tar_backup.yaml',
)
FULL_RE = re.compile(r'full\.(?P<full>\d+)\.tgz')
INCR_RE = re.compile(r'incr\.(?P<full>\d+)\.(?P<incr>\d+)\.tgz')
ANY_RE = re.compile(r'\D+\.(?P<full>\d+).+(tgz|snar)')
LOCKFILE_PATH = '/tmp/.tar_backup.lock'


class ScriptAlreadyRunning(Exception):
    pass


@contextlib.contextmanager
def flock(path):
    """Creates a lock to ensure we run only one copy of the application.
    Based on http://code.activestate.com/recipes/576572/

    Args:
        path: full path to lock file.

    """
    try:
        fd = os.open(path, os.O_CREAT | os.O_EXCL | os.O_RDWR)
    except OSError, e:
        raise ScriptAlreadyRunning("The script is already running")

    try:
        yield fd
    finally:
        os.close(fd)
        os.unlink(path)


def get_files_matching_re(file_list, regex):
    """In a list of files, yields all names that match regex.

    Args:
        file_list: list of file names
        regex: re.compile() object

    Returns:
        iterator returning all file names that match regex.

    """
    for file_name in file_list:
        m = regex.match(file_name)
        if m:
            yield m.groupdict()


def get_new_backup_index(dst_path, keep_full, keep_incr):
    """Returns the next backup level for this directory by
    analyzing existing file names in this dir.

    Args:
        dst_path: destination dir for backups
        keep_full: how many full copies to keep
        keep_incr: how many incremental copies to keep

    Returns:
        If the next backup should be full: {'full': 2}, where 2 is full backup number
        If the next backup should be incremental: {'full': 2, 'incr': 0}, e.g. full backup index and incr backup index

    """
    last_backup = {}
    new_backup = {}

    file_list = os.listdir(dst_path)
    #   Handling the case when we don't make incrementals at all
    keep_incr = keep_incr - 1 if keep_incr > 0 else 0

    #   Find latest full backup
    for m_dict in get_files_matching_re(file_list, FULL_RE):
        last_backup.setdefault('full', m_dict['full'])
        if int(m_dict['full']) > int(last_backup['full']):
            last_backup['full'] = m_dict['full']

    #   Find latest incremental for lastest full backup
    for m_dict in get_files_matching_re(file_list, INCR_RE):
        if m_dict['full'] == last_backup['full']:
            last_backup.setdefault('incr', m_dict['incr'])
            if int(m_dict['incr']) > int(last_backup['incr']):
                last_backup['incr'] = m_dict['incr']

    current_full, current_incr = int(last_backup.get('full', -1)), int(last_backup.get('incr', -1))
    new_full, new_incr = current_full, current_incr

    #   There were no previous backups
    if current_full == -1:
        new_backup = {'full': 0}
        return new_backup

    #   Do we need a new full?
    if current_incr + 1 > keep_incr or keep_incr == 0:
        new_full = current_full + 1
        new_incr = -1
    else:
        #   Otherwise, make an incremental
        new_incr = current_incr + 1

    new_backup['full'] = new_full
    if new_incr != -1:
        new_backup['incr'] = new_incr

    return new_backup


def clean_old_backups(last_full, dry_run, **kwargs):
    """Cleans older backup files.

    Args:
        last_full: index of last full backup cycle 
        dry_run: True if we shouldn't actually delete files
        kwargs: dict derived from config file

    """
    dst_dir = kwargs['backup_to']
    keep_full = kwargs['keep_full_copies']

    for file_name in os.listdir(dst_dir):
        m = ANY_RE.match(file_name)
        if m:
            m_dict = m.groupdict()
            if int(last_full) - int(m_dict['full']) >= keep_full:
                file_path = os.path.join(dst_dir, file_name)
                print 'Deleting {0}'.format(file_path)
                if not dry_run:
                    os.unlink(file_path)


def run_command(cmd):
    """Runs a command via subprocess (shell=True, eh)

    """
    print "Running {0}".format(cmd)
    c = subprocess.Popen(
        cmd,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )
    stdout, stderr = c.communicate()
    ret = c.returncode
    if ret < 0:
        print "Command was terminated by external UNIX signal {0}".format(ret)
    elif ret > 0:
        print "Command exited with returncode {0}".format(ret)
        print "Command stdout:"
        print stdout
        print "Command stderr:"
        print stderr
    else:
        print "Command executed succesfully"


def launch_tar(dst_file_name, snar_file_name, dry_run, **kwargs):
    """Launches tar to perform backup.

    Args:
        dst_file_name: Archive file name (full path!)
        snar_file_name: .snar file name, which contains snapshot data (full path as well)
        dry_run: True if this is a dry run and we shouldn't launch tar
        kwargs: dict derived from config file

    """
    tar_args = ['tar', 'cvzf', os.path.join(kwargs['backup_to'], dst_file_name)]
    for exclude_dir in kwargs.get('exclude', []):
        tar_args.append('--exclude')
        tar_args.append(exclude_dir)

    if kwargs.get('do_not_cross_mount_points', False):
        tar_args.append('--one-file-system')

    tar_args.append('-g')
    tar_args.append(os.path.join(kwargs['backup_to'], snar_file_name))
    tar_args.append(kwargs['path'])

    print ' '.join(tar_args)

    if dry_run:
        return

    print "Launching tar now"
    tar = subprocess.Popen(
        tar_args,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )

    #stdout, stderr = tar.communicate()
    #   We better handle this line-by-line
    for line in iter(tar.stdout.readline, ''):
        print line.rstrip()


def do_backup(dry_run = False, **kwargs):
    """Backups one directory.

    Args: 
        dry_run: True if we shouldn't perform the actual backup
        kwargs: config dict for this directory

    """
    #   Check mount point first
    mount_point = kwargs.get('check_mount_point')
    if mount_point:
        if not os.path.ismount(mount_point):
            print "{0} isn't mounted, halting backup".format(mount_point)
            return

    #   Preflight scripts
    for cmd in kwargs.get('preflight_cmds', []):
        run_command(cmd)

    dst_path = kwargs['backup_to']
    new_backup_index = get_new_backup_index(
        dst_path,
        kwargs['keep_full_copies'],
        kwargs['keep_incremental_copies'],
    )

    if 'incr' in new_backup_index.keys():
        dst_file_name = 'incr.{full}.{incr}.tgz'.format(**new_backup_index)
    else:
        dst_file_name = 'full.{full}.tgz'.format(**new_backup_index)

    snar_file_name = 'cycle.{full}.snar'.format(**new_backup_index)
    launch_tar(dst_file_name, snar_file_name, dry_run, **kwargs)

    clean_old_backups(new_backup_index['full'], dry_run, **kwargs)

    #   Postflight scripts
    for cmd in kwargs.get('postflight_cmds', []):
        run_command(cmd)


def parse_config(config_file_path):
    """Parses configuration file.

    Args:
        config_file_path: Path to YAML config file

    Returns:
        Nested object, containing configuration parameters

    """
    with open(config_file_path, 'r') as f:
        return yaml.load(f)


def parse_args():
    """Parses command-line arguments.

    """
    parser = argparse.ArgumentParser(
        description=__doc__,
    )   

    #   Add arguments below
    parser.add_argument(
        '--dry-run',
        action='store_true',
        help='Don\'t actually perform backup',
    )

    return parser.parse_args()


def main():
    #   Read config first
    try:
        conf = parse_config(CONFIG_FILE_PATH)
    except IOError:
        print "Can't access config file at {0}, is it present?".format(CONFIG_FILE_PATH)
        return 1
    except yaml.scanner.ScannerError as e:
        print "There is an error in your YAML syntax:"
        print e
        return 2

    #   Parsing args
    args = parse_args()

    try:
        with flock(LOCKFILE_PATH):
            for backup_params in conf['dirs_to_backup']:
                assert(backup_params['keep_full_copies'] >= 1)
                assert(os.path.isdir(backup_params['path']))
                assert(os.path.isdir(backup_params['backup_to']))

                print "Backing up {path}".format(**backup_params)
                do_backup(dry_run=args.dry_run, **backup_params)
                print
    except ScriptAlreadyRunning as e:
        print e
        return 3

    return 0


if __name__ == '__main__':
    sys.exit(main())

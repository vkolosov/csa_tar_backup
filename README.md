A simple tar backup script
==========================

Synopsis
--------

This script handles creation of full and incremental copies using GNU tar. It uses a configuration file to determine which directories should be backed up, where backups should be stored, and how many full/incremental copies should be kept, plus some extra options like checking validity of a mount point prior to backing up data. Backup files will look similar to this:

    cycle.10.snar
    cycle.9.snar
    full.10.tgz
    full.9.tgz
    incr.9.0.tgz
    incr.9.1.tgz
    incr.9.2.tgz

In this example, two full copies plus three incrementals for each full copy are kept. `.snar` files are filesystem snapshots used by tar to determine which files should be included in incremental copy. Numbers in file names allow you to tell between different backup cycles (cycle refers to a full copy plus all incrementals for this full copy).

Usage is as simple as running:

    ./tar_backup.py

(but set up your config first!)

Config files parameters
-----------------------

There's an example configuration file (`tar_backup.yaml.example`). Actual configuration file should be called `tar_backup.yaml` and reside in the same directory as `tar_backup.py`. Currently, there's only one configuration section called `dirs_to_backup`, which contains a list of directories that are backed up. For each directory, following parameters can be set:

+ `path`: full path to directory which is backed up (e.g. `'/'` or `'/home/user'`)
+ `backup_to`: full path to directory where backup files will be put. 
+ `exclude`: list of directories to be excluded from backup (add stuff like `'/proc'`, `'/tmp'` here). Optional.
+ `do_not_cross_mount_points`: if set to True, tar will not cross current file system boundaries (`--one-file-system` option). Optional.
+ `keep_full_copies`: how many full copies to keep.
+ `keep_incremental_copies`: how many incremental copies to keep.
+ `check_mount_point`: script will check if specified path is a mount point prior to running backup.
+ `preflight_cmds`: list of commands to run prior to backing up data. Optional.
+ `postflight_cmds`: list of commands to run after backup is finished. Optional.

Config file example
-------------------

Backs up server root directory:

    #   Config
    dirs_to_backup:
        - path: /
          backup_to: /fileserver/backups/serverX
          exclude:
            - /proc
            - /mnt
            - /run
            - /tmp
            - /var/run
            - /var/spool/apt-mirror
          do_not_cross_mount_points: True
          keep_full_copies: 2
          keep_incremental_copies: 7
          check_mount_point: /fileserver
          preflight_cmds:
            - sudo -u postgres pg_dumpall --clean > /var/backups/postgres/postgres_backup.sql
            - /opt/scripts/mysql_backup.sh
          postflight_cmds:
            - /opt/scripts/rsync_backups_to_fileserver2.sh

Command-line options
--------------------

+ `--dry-run`: no files will be backed up or deleted. Useful to check which tar command will be issued when running actual backup. However, pre- and postflight scripts will still be run.

